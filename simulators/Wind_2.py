from PySAM.PySSC import PySSC

ssc = PySSC()

class Simulator(object):
    def __init__(self):
        pass

    def start(self):
        ssc.module_exec_set_print(0)
        data = ssc.data_create()
        ssc.data_set_string(data, b'wind_resource_filename',
                            b'data/CA Southern-Mountainous.srw');
        ssc.data_set_number(data, b'wind_resource_shear', 0.14000000059604645)
        ssc.data_set_number(data, b'wind_resource_turbulence_coeff', 0.10000000149011612)
        ssc.data_set_number(data, b'system_capacity', 75200)
        ssc.data_set_number(data, b'wind_resource_model_choice', 0) #Zero would do the simulation with resource file
        ssc.data_set_number(data, b'weibull_reference_height', 50)
        ssc.data_set_number(data, b'weibull_k_factor', 2)
        ssc.data_set_number(data, b'weibull_wind_speed', 5)
        ssc.data_set_number(data, b'wind_turbine_rotor_diameter', 82)
        wind_turbine_powercurve_windspeeds = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21,
                                              22,
                                              23, 24, 25, 26, 27, 28, 29, 30];
        ssc.data_set_array(data, b'wind_turbine_powercurve_windspeeds', wind_turbine_powercurve_windspeeds);
        wind_turbine_powercurve_powerout = [0, 0, 25, 82, 174, 321, 532, 815, 1180, 1580, 1890, 2100, 2250, 2350, 2350,
                                            2350, 2350, 2350, 2350, 2350, 2350, 2350, 2350, 2350, 2350, 0, 0, 0, 0, 0];
        ssc.data_set_array(data, b'wind_turbine_powercurve_powerout', wind_turbine_powercurve_powerout);
        ssc.data_set_number(data, b'wind_turbine_hub_ht', 80)
        ssc.data_set_number(data, b'wind_turbine_max_cp', 0.44999998807907104)
        wind_farm_xCoordinates = [0, 656, 1312, 1968, 2624, 3280, 3936, 4592, 328, 984, 1640, 2296, 2952, 3608, 4264,
                                  4920,
                                  0, 656, 1312, 1968, 2624, 3280, 3936, 4592, 328, 984, 1640, 2296, 2952, 3608, 4264,
                                  4920];
        ssc.data_set_array(data, b'wind_farm_xCoordinates', wind_farm_xCoordinates);
        wind_farm_yCoordinates = [0, 0, 0, 0, 0, 0, 0, 0, 656, 656, 656, 656, 656, 656, 656, 656, 1312, 1312, 1312,
                                  1312,
                                  1312, 1312, 1312, 1312, 1968, 1968, 1968, 1968, 1968, 1968, 1968, 1968];
        ssc.data_set_array(data, b'wind_farm_yCoordinates', wind_farm_yCoordinates);
        ssc.data_set_number(data, b'wind_farm_losses_percent', 0)
        ssc.data_set_number(data, b'wind_farm_wake_model', 0)
        ssc.data_set_number(data, b'adjust:constant', 0)
        module = ssc.module_create(b'windpower')
        ssc.module_exec_set_print(0);
        if ssc.module_exec(module, data) == 0:
            print('windpower simulation error')
            idx = 1
            msg = ssc.module_log(module, 0)
            while (msg != None):
                print('	: ' + msg.decode("utf - 8"))
                msg = ssc.module_log(module, idx)
                idx = idx + 1
            SystemExit("Simulation Error");
        ssc.module_free(module)
        annual_energy = ssc.data_get_number(data, b'annual_energy');
        #print('Annual energy (year 1) = ', annual_energy)
        capacity_factor = ssc.data_get_number(data, b'capacity_factor');
        #print('Capacity factor (year 1) = ', capacity_factor)
        system_power_generated = ssc.data_get_array(data, b'gen');
        # print('System Power Generated = ', system_power_generated, len(system_power_generated))
        ssc.data_free(data);
        
        return system_power_generated

if __name__ == "__main__":
    print('Current folder = H:/sam-sdk-2017-9-5-r4/First Wind project')
    print('SSC Version = ', ssc.version())
    print('SSC Build Information = ', ssc.build_info().decode("utf - 8"))
    
    Simulator().start()